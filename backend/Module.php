<?php

namespace Coppel\RAC\Modules;

use PDO;
use Phalcon\Mvc\Micro\Collection;

class Module implements IModule
{
    public function __construct()
    {
    }

    public function registerLoader($loader)
    {
        $loader->registerNamespaces([
            'Rinku\Nomina\Controllers' => __DIR__ . '/controllers/',
            'Rinku\Nomina\Models' => __DIR__ . '/models/'
        ], true);
    }

    public function getCollections()
    {
        $empleados = new Collection();

        $empleados->setPrefix('/api')
            ->setHandler('\Rinku\Nomina\Controllers\EmpleadoController')
            ->setLazy(true);
        
        $empleados->get('/empleados', 'obtenerEmpleados');
        $empleados->get('/empleados/{id}', 'obtenerEmpleadoPorId');
        $empleados->post('/empleados', 'guardarEmpleado');
        $empleados->put('/empleados/{id}', 'actualizarEmpleado');
        $empleados->delete('/empleados/{id}', 'eliminarEmpleado');

        $roles = new Collection();
        $roles->setPrefix('/api')
            ->setHandler('\Rinku\Nomina\Controllers\RolesController')
            ->setLazy(true);

        $roles->get('/roles', 'obtenerRoles');
        $roles->get('/roles/{id}', 'obtenerRolesPorId');

        $tipos = new Collection();
        $tipos->setPrefix('/api')
            ->setHandler('\Rinku\Nomina\Controllers\TiposController')
            ->setLazy(true);

        $tipos->get('/tipos', 'obtenerTiposEmpleado');
        $tipos->get('/tipos/{id}', 'obtenerTipoEmpleadoPorId');

        $movimientos = new Collection();
        $movimientos->setPrefix('/api')
            ->setHandler('\Rinku\Nomina\Controllers\MovimientosController')
            ->setLazy(true);
        $movimientos->get('/movimientos', 'obtenerMovimientos');
        $movimientos->get('/movimientos/empleado/{id}/mes/{mes}/anio/{anio}', 'obtenerMovimientoPorId');
        $movimientos->post('/movimientos', 'guardarMovimiento');

        return [
            $empleados, $roles, $tipos, $movimientos
        ];
    }

    public function registerServices()
    {
        $di = \Phalcon\DI::getDefault();

        $di->set('conexion', function () use ($di) {
            $config = $di->get('config')->db;

            return new PDO(
                "mysql:host={$config->host};dbname={$config->dbname};",
                $config->username,
                $config->password, [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
                ]
            );
        });

        $di->set('logger', function () {
            return new \Katzgrau\KLogger\Logger('logs');
        });
    }
}
