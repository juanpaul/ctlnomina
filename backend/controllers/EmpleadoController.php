<?php

namespace Rinku\Nomina\Controllers;

use Coppel\RAC\Controllers\RESTController;
use Coppel\RAC\Exceptions\HTTPException;
use Rinku\Nomina\Models as Modelos;

class EmpleadoController extends RESTController
{
    private $logger;
    private $modelo;

    public function onConstruct()
    {
        $this->logger = \Phalcon\DI::getDefault()->get('logger');
        $this->modelo = new Modelos\EmpleadoModel();
    }

    public function obtenerEmpleados()
    {
        $response = null;

        try {
            $response = $this->modelo->obtenerEmpleados();
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");

            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }

        return $this->respond(['empleados' => $response]);
    }

    public function obtenerEmpleadoPorId($id)
    {
        $response = null;

        try {
            $response = $this->modelo->obtenerEmpleadoPorId($id);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");

            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }

        return $this->respond(['empleado' => $response]);
    }

    public function guardarEmpleado()
    {
        $response = false;
        $empleado = null;

        try {
            $empleado = $this->request->getJsonRawBody();
            $response = $this->modelo->guardarEmpleado($empleado);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");

            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }

        return $this->respond(['guardó' => $response]);
    }

    public function actualizarEmpleado($id)
    {
        $response = false;
        $empleado = null;

        try {
            if ($this->modelo->obtenerEmpleadoPorId($id)==null) {
                throw new \Coppel\RAC\Exceptions\HTTPException(
                    'El empleado no existe.',
                    404,
                    array()
                );
            }
            $empleado = $this->request->getJsonRawBody();
            $response = $this->modelo->actualizarEmpleado($id, $empleado);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");

            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }

        return $this->respond(['actualizó' => $response]);
    }

    public function eliminarEmpleado($id)
    {
        $response = false;
        $empleado = null;

        try {
            if ($this->modelo->obtenerEmpleadoPorId($id)==null) {
                throw new \Coppel\RAC\Exceptions\HTTPException(
                    'El empleado no existe.',
                    404,
                    array()
                );
            }
            $response = $this->modelo->eliminarEmpleado($id);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");

            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }

        return $this->respond(['eliminó' => $response]);
    }
}
