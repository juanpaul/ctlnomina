<?php

namespace Rinku\Nomina\Controllers;

use Coppel\RAC\Controllers\RESTController;
use Coppel\RAC\Exceptions\HTTPException;
use Rinku\Nomina\Models as Modelos;

class MovimientosController extends RESTController
{
    private $logger;
    private $modelo;

    public function onConstruct()
    {
        $this->logger = \Phalcon\DI::getDefault()->get('logger');
        $this->modelo = new Modelos\MovimientosModel();
    }

    public function obtenerMovimientos()
    {
        $response = null;

        try {
            $response = $this->modelo->obtenerMovimientos();
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");

            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }

        return $this->respond(['movimientos' => $response]);
    }

    public function obtenerMovimientoPorId($empleado, $mes, $anio)
    {
        $response = null;

        try {
            $response = $this->modelo->obtenerMovimientoPorId($empleado, $mes, $anio);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");

            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }

        return $this->respond(['movimiento' => $response]);
    }

    public function guardarMovimiento()
    {
        $response = null;
        $movimiento = null;

        try {
            $movimiento = $this->request->getJsonRawBody();
            $response = $this->modelo->guardarMovimiento($movimiento);
        } catch (\Exception $ex) {
            $mensaje = $ex->getMessage();
            $this->logger->error('['. __METHOD__ ."] Se lanzó la excepción > $mensaje");

            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500, [
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                ]
            );
        }

        return $this->respond(['guardo' => $response]);
    }
}
