<?php

namespace Rinku\Nomina\Models;

use Phalcon\Mvc\Model as Modelo;

class EmpleadoModel extends Modelo
{
    public function obtenerEmpleados()
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $db = $di->get('conexion');

        $statement = $db->prepare("SELECT e.id, e.nombre, e.apellido, r.nombre AS rol, te.nombre AS tipo 
                                   FROM empleados e 
                                     INNER JOIN roles r ON e.rol = r.id 
                                     INNER JOIN tiposempleado te ON e.tipo = te.id
                                    ORDER BY e.id;");
        $statement->execute();
        
        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $empleado = new \stdClass();
            $empleado->id = $entry["id"];
            $empleado->nombre = $entry["nombre"];
            $empleado->apellido = $entry["apellido"];
            $empleado->rol = $entry["rol"];
            $empleado->tipo = $entry["tipo"];
            $response[] = $empleado;
        }
        
        return $response;
    }

    public function obtenerEmpleadoPorId($id)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $db = $di->get('conexion');

        $statement = $db->prepare("SELECT id, nombre, apellido, rol, tipo 
                                   FROM empleados  
                                   WHERE id = ?;");
        $statement->bindParam(1, $id, \PDO::PARAM_INT);
        $statement->execute();
        
        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $empleado = new \stdClass();
            $empleado->id = $entry["id"];
            $empleado->nombre = $entry["nombre"];
            $empleado->apellido = $entry["apellido"];
            $empleado->rol = $entry["rol"];
            $empleado->tipo = $entry["tipo"];
            $response[] = $empleado;
        }

        return $response;
    }

    public function guardarEmpleado($empleado)
    {
        $response = false;
        $di = \Phalcon\DI::getDefault();
        $db = $di->get('conexion');

        $statement = $db->prepare("INSERT INTO empleados (nombre, apellido, rol, tipo) 
                                    VALUES(?, ?, ?, ?);");
        $statement->bindParam(1, $empleado->nombre, \PDO::PARAM_STR);
        $statement->bindParam(2, $empleado->apellido, \PDO::PARAM_STR);
        $statement->bindParam(3, $empleado->rol, \PDO::PARAM_INT);
        $statement->bindParam(4, $empleado->tipo, \PDO::PARAM_INT);
        $response = $statement->execute();

        return $response;
    }

     public function actualizarEmpleado($id, $empleado)
    {
        $response = false;
        $di = \Phalcon\DI::getDefault();
        $db = $di->get('conexion');

        $statement = $db->prepare("UPDATE empleados 
                                    SET nombre = ? , apellido = ?, rol = ?, tipo = ? 
                                    WHERE id = ?;");
        $statement->bindParam(1, $empleado->nombre, \PDO::PARAM_STR);
        $statement->bindParam(2, $empleado->apellido, \PDO::PARAM_STR);
        $statement->bindParam(3, $empleado->rol, \PDO::PARAM_INT);
        $statement->bindParam(4, $empleado->tipo, \PDO::PARAM_INT);
        $statement->bindParam(5, $id, \PDO::PARAM_INT);
        $response = $statement->execute();

        return $response;
    }

    public function eliminarEmpleado($id)
    {
        $response = false;
        $di = \Phalcon\DI::getDefault();
        $db = $di->get('conexion');

        $statement = $db->prepare("DELETE FROM empleados WHERE id =  ?;");
        $statement->bindParam(1, $id, \PDO::PARAM_INT);
        $response = $statement->execute();

        return $response;
    }


}
