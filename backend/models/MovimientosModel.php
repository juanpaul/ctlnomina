<?php

namespace Rinku\Nomina\Models;

use Phalcon\Mvc\Model as Modelo;

class MovimientosModel extends Modelo
{
    public function obtenerMovimientos()
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;

        $db = $di->get('conexion');

        $statement = $db->prepare("SELECT id, empleado, fecha, entregas, rol_cubierto
                                    FROM movimientos;");
        $statement->execute();

        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $movimiento = new \stdClass();
            $movimiento->id = $entry["id"];
            $movimiento->empleado = $entry["empleado"];
            $movimiento->fecha = $entry["fecha"];
            $movimiento->entregas = $entry["entregas"];
            $movimiento->rolCubierto = $entry["rol_cubierto"];
            $response[] = $movimiento;
        }

        return $response;
    }

    public function obtenerMovimientoPorId($empleado, $mes, $anio)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;

        $db = $di->get('conexion');

        $statement = $db->prepare("SELECT id, empleado, fecha, entregas, rol_cubierto 
                                    FROM movimientos 
                                    WHERE empleado = ? 
                                        AND MONTH(fecha) = ? 
                                        AND YEAR(fecha) = ?;");
        $statement->bindParam(1, $empleado, \PDO::PARAM_INT);
        $statement->bindParam(2, $mes, \PDO::PARAM_INT);
        $statement->bindParam(3, $anio, \PDO::PARAM_INT);
        $statement->execute();

        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $movimiento = new \stdClass();
            $movimiento->id = $entry["id"];
            $movimiento->empleado = $entry["empleado"];
            $movimiento->fecha = $entry["fecha"];
            $movimiento->entregas = $entry["entregas"];
            $movimiento->rolCubierto = $entry["rol_cubierto"];
            $response[] = $movimiento;
        }

        return $response;
    }

    public function guardarMovimiento($movimiento)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;

        $db = $di->get('conexion');

        $statement = $db->prepare("INSERT INTO movimientos(empleado, fecha, entregas, rol_cubierto)
                                    VALUES (?, ?, ?, ?);");
        $statement->bindParam(1, $movimiento->empleado, \PDO::PARAM_INT);
        $statement->bindParam(2, $movimiento->fecha, \PDO::PARAM_STR);
        $statement->bindParam(3, $movimiento->entregas, \PDO::PARAM_INT);
        $statement->bindParam(4, $movimiento->rolCubierto, \PDO::PARAM_INT);
        $response = $statement->execute();

        return $response;
    }
}
