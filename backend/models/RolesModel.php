<?php

namespace Rinku\Nomina\Models;

use Phalcon\Mvc\Model as Modelo;

class RolesModel extends Modelo
{
    public function obtenerRoles()
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $db = $di->get('conexion');

        $statement = $db->prepare("SELECT id, nombre FROM roles;");
        $statement->execute();

        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $rol = new \stdClass();
            $rol->id = $entry["id"];
            $rol->nombre = $entry["nombre"];
            $response[] = $rol;
        }

        return $response;
    }

    public function obtenerRolesPorId($id)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $db = $di->get('conexion');

        $statement = $db->prepare("SELECT id, nombre FROM roles WHERE id = ?;");
        $statement->bindParam(1, $id, \PDO::PARAM_INT);
        $statement->execute();

        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $rol = new \stdClass();
            $rol->id = $entry["id"];
            $rol->nombre = $entry["nombre"];
            $response[] = $rol;
        }

        return $response;
    }
}
