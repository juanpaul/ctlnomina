<?php

namespace Rinku\Nomina\Models;

use Phalcon\Mvc\Model as Modelo;

class TiposModel extends Modelo
{
    public function obtenerTiposEmpleado()
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $db = $di->get('conexion');

        $statement = $db->prepare("SELECT id, nombre FROM tiposempleado;");
        $statement->execute();
        
        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $tipo = new \stdClass();
            $tipo->id = $entry["id"];
            $tipo->nombre = $entry["nombre"];
            $response[] = $tipo;
        }

        return $response;
    }

    public function obtenerTipoEmpleadoPorId($id)
    {
        $di = \Phalcon\DI::getDefault();
        $response = null;
        $db = $di->get('conexion');

        $statement = $db->prepare("SELECT id, nombre FROM tiposempleado WHERE id = ?;");
        $statement->bindParam(1, $id, \PDO::PARAM_INT);
        $statement->execute();
        
        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $tipo = new \stdClass();
            $tipo->id = $entry["id"];
            $tipo->nombre = $entry["nombre"];
            $response[] = $tipo;
        }

        return $response;
    }
}
