import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { EmpleadosComponent } from './empleados/empleados.component';
import { EmpleadosFormComponent } from './empleados/empleados-form/empleados-form.component';
import { MovimientosComponent } from './empleados/movimientos/movimientos.component';
import { EmpleadosModule } from './empleados/empleados.module';

export const routes: Routes = [
    {
        path: '',
        redirectTo: '/empleados',
        pathMatch: 'full'
    },
    {
        path: 'empleados',
        component: EmpleadosComponent,
        loadChildren: './empleados/empleados.module#EmpleadosModule'
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes),
        CommonModule,
        EmpleadosModule
    ],
    exports: [
        RouterModule
    ],
    declarations: [
    ]
})
export class AppRoutingModule { }
