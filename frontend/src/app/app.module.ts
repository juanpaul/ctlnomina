import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { EmpleadosComponent } from './empleados/empleados.component';
import { HttpClientModule } from '@angular/common/http';
import { EmpleadosFormComponent } from './empleados/empleados-form/empleados-form.component';
import { FormBuilder, FormsModule, FormControl, ReactiveFormsModule } from '@angular/forms';
import { ToasterModule } from 'angular2-toaster';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPaginationModule } from 'ngx-pagination';
import { MovimientosComponent } from './empleados/movimientos/movimientos.component';
import { PaginationModule } from '../../node_modules/ngx-bootstrap';
import { TooltipModule } from 'ngx-bootstrap';

@NgModule({
    declarations: [
        AppComponent,
        EmpleadosComponent,
        EmpleadosFormComponent,
        MovimientosComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        ToasterModule.forRoot(),
        NgxPaginationModule,
        PaginationModule.forRoot(),
        TooltipModule.forRoot()
    ],
    providers: [FormBuilder], 
    bootstrap: [AppComponent]
})
export class AppModule { }
