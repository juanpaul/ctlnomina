export class Empleado {
    id?: number;
    nombre: string;
    apellido: string;
    rol?: number;
    tipo?: number;
}
