import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from '../../../../node_modules/rxjs';
import { Empleado } from '../Models/empleado';
import { Movimiento } from '../Models/movimiento';

@Injectable({
    providedIn: 'root'
})
export class EmpleadosService {

    public empleadoURL = "http://localhost/backend/api/empleados";
    public rolesURL = "http://localhost/backend/api/roles";
    public tiposURL = "http://localhost/backend/api/tipos";
    public movimientosURL = "http://localhost/backend/api/movimientos";

    constructor(private http: HttpClient) { }

    obtenerEmpleados(): Observable<any> {
        return this.http.get(this.empleadoURL);
    }

    obtenerRoles(): Observable<any> {
        return this.http.get(this.rolesURL);
    }

    obtenerTiposEmpleado(): Observable<any> {
        return this,this.http.get(this.tiposURL);
    }

    obtenerEmpleadoPorId(id: number): Observable<any> {
        return this.http.get(this.empleadoURL + '/' + id);
    }

    guardarEmpleado(empleado: Empleado): Observable<any> {
        return this.http.post(this.empleadoURL, empleado);
    }

    actualizarEmpleado(empleado: Empleado): Observable<any> {
        return this.http.put(this.empleadoURL + '/' + empleado.id, empleado);
    }

    eliminarEmpleado(id: number): Observable<any> {
        return this.http.delete(this.empleadoURL + '/' + id);
    }

    obtenerMovimiento(empleado:number, mes: number, anio: number) : Observable<any> {
        return this.http.get(this.movimientosURL + '/empleado/' + empleado + '/mes/' + mes + '/anio/' + anio);
    }

    guardarMovimiento(movimiento: Movimiento): Observable<any> {
        return this.http.post(this.movimientosURL, movimiento);
    }

}
