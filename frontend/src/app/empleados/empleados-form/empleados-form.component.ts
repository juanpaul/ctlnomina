import { Component, OnInit, Input } from '@angular/core';
import { Empleado } from '../Models/empleado';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import { ToasterService, Toast } from 'angular2-toaster';

import { EmpleadosService } from '../Services/empleados.service';
import { Rol } from '../Models/rol';
import { Tipo } from '../Models/tipo';

@Component({
    selector: 'app-empleados-form',
    templateUrl: './empleados-form.component.html',
    styleUrls: ['./empleados-form.component.css']
})
export class EmpleadosFormComponent implements OnInit {

    public title: string;
    public empleado: Empleado;
    public roles: Rol[];
    public tipos: Tipo[];

    //private sub: any;
    private id: number;
    private toasterService: ToasterService;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private empleadosServicio: EmpleadosService,
        toasterService: ToasterService,
    ) {
        this.title = "Captura de Empleado";
        this.empleado = { nombre: '', apellido: '', rol: null, tipo: null };
        this.toasterService = toasterService;
    }

    ngOnInit() {
        this.obtenerRoles();
        
        this.obtenerTipos();

        this.route.params.subscribe((params) => {
            this.id =  +params['id'];
            this.title = this.id ? 'Editar empleado ' + this.id : 'Nuevo empleado';
            if (this.id) {
                //this.blockUI.start('Loading...');
                this.empleadosServicio.obtenerEmpleadoPorId(this.id).subscribe((resultado) => {
                    this.empleado = resultado.data.empleado[0];
                    //this.blockUI.stop();
                });
            }
        });
    }

    obtenerRoles() {
        this.empleadosServicio.obtenerRoles().subscribe((resultado) => {
            this.roles = resultado.data.roles;
        });
    }

    obtenerTipos() {
        this.empleadosServicio.obtenerTiposEmpleado().subscribe((resultado) => {
            this.tipos = resultado.data.tipos;
        });
    }

    onSubmit(){
        if (this.id) {
            this.empleadosServicio.actualizarEmpleado(this.empleado).subscribe((resultado) => {
                this.goBack();
                var toast: Toast = {
                    type: 'success',
                    title: '¡Excelente!',
                    body: 'Empleado actualizado con éxito',
                    showCloseButton: true
                };
                this.toasterService.pop(toast);
                
            }, (error) => {
                var toast: Toast = {
                    type: 'error',
                    title: '¡Ups!',
                    body: 'Algo salió mal',
                    showCloseButton: true
                };
                this.toasterService.pop(toast);
            });
        }
        else {
            this.empleadosServicio.guardarEmpleado(this.empleado).subscribe((resultado) => {
                this.goBack();
                var toast: Toast = {
                    type: 'success',
                    title: '¡Excelente!',
                    body: 'Empleado guardado con éxito',
                    showCloseButton: true
                };
                this.toasterService.pop(toast);
            }, (error) => {
                var toast: Toast = {
                    type: 'error',
                    title: 'Ups',
                    body: 'Algo salió mal',
                    showCloseButton: true
                };
                this.toasterService.pop(toast);
            });
        }
    }

    goBack() {
        this.router.navigate(['/empleados']);
    }

}
