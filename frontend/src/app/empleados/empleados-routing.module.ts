import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpleadosComponent } from './empleados.component';
import { EmpleadosFormComponent } from './empleados-form/empleados-form.component';
import { MovimientosComponent } from './movimientos/movimientos.component';

const APP_ROUTES: Routes = [
    {
        path: 'empleados',
        data: {
            title: 'Empleados',
        },
        component: EmpleadosComponent,
    },
    {
        path: 'nuevo',
        data: {
            title: 'Nuevo',
        },
        component: EmpleadosFormComponent,
    },
    {
        path: 'editar/:id',
        data: {
            title: 'Editar',
        },
        component: EmpleadosFormComponent,
    },
    {
        path: 'movimientos/:id',
        data: {
            title: 'Movimientos',
        },
        component: MovimientosComponent,
    }
];
@NgModule({
    imports: [RouterModule.forChild(APP_ROUTES)],
    exports: [RouterModule]
})
export class EmpleadosRoutingModule { }
