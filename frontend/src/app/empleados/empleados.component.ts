import { Component, OnInit } from '@angular/core';
import { EmpleadosService } from './Services/empleados.service';
import { Empleado } from './Models/empleado';
import { ToasterService, Toast } from '../../../node_modules/angular2-toaster';
import { PageChangedEvent } from '../../../node_modules/ngx-bootstrap/pagination';
import swal from 'sweetalert2';

@Component({
    selector: 'app-empleados',
    templateUrl: './empleados.component.html',
    styleUrls: ['./empleados.component.css']
})
export class EmpleadosComponent implements OnInit {
    public columnas: any[];
    public empleados: Empleado[];
    public p: number = 1;
    public collection: any[] = this.empleados;
    public totalElementos: number;
    public elementosPorPagina: number;
    public empleadoPaginado: Empleado[];

    private toasterService: ToasterService;

    constructor(
        private empleadosService: EmpleadosService,
        toasterService: ToasterService
    ) {
        this.toasterService = toasterService;

        this.elementosPorPagina = 10;
    }

    ngOnInit() {
        this.columnas = [
            { campo: 'id', cabecera: '#' },
            { campo: 'nombre', cabecera: 'Nombre' },
            { campo: 'apellido', cabecera: 'Apellido' },
            { campo: 'rol', cabecera: 'Rol' },
            { campo: 'tipo', cabecera: 'Tipo' },
        ];

        this.cargarEmpleados();


    }

    pageChanged(event: PageChangedEvent): void {
        const startItem = (event.page - 1) * event.itemsPerPage;
        const endItem = event.page * event.itemsPerPage;
        this.empleadoPaginado = this.empleados.slice(startItem, endItem);
    }

    cargarEmpleados() {
        this.empleadosService.obtenerEmpleados().subscribe((resultado) => {
            this.empleados = resultado.data.empleados;
            this.totalElementos = this.empleados.length;
            this.empleadoPaginado = this.empleados.slice(0, 10);
        });
    }

    onDelete(id: number) {
        swal({
            title: '¿Seguro que desea eliminarlo?',
            text: "Este proceso no es reversible",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminalo'
        }).then((respuesta) => {
            if (respuesta.value) {
                this.empleadosService.eliminarEmpleado(id).subscribe((resultado) => {
                    swal(
                        '¡Eliminado!',
                        'El empleado ha sido eliminado.',
                        'success'
                    )
                    this.cargarEmpleados();
                    var toast: Toast = {
                        type: 'success',
                        title: '¡Listo¡',
                        body: 'Empleado eliminado con éxito',
                        showCloseButton: true
                    };
                    this.toasterService.pop(toast);
                }, (error) => {
                    var toast: Toast = {
                        type: 'error',
                        title: '¡Ups¡',
                        body: 'Algo salió mal',
                        showCloseButton: true
                    };
                    this.toasterService.pop(toast);
                });
            }
        })
    }
}
