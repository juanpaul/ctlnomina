import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmpleadosRoutingModule } from './empleados-routing.module';
import { PaginationModule } from 'ngx-bootstrap';
import { EmpleadosService } from './Services/empleados.service';

@NgModule({
    imports: [
        CommonModule,
        EmpleadosRoutingModule,
        PaginationModule.forRoot()
    ],
    declarations: [
    ],
    providers: [
        EmpleadosService
    ]
})
export class EmpleadosModule {}
