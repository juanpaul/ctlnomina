import { Component, OnInit } from '@angular/core';
import { Movimiento } from '../Models/movimiento';
import { EmpleadosService } from '../Services/empleados.service';
import { Empleado } from '../Models/empleado';
import { ToasterService, Toast } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';
import { Rol } from '../Models/rol';
import { Tipo } from '../Models/tipo';
import swal from 'sweetalert2';

@Component({
    selector: 'app-movimientos',
    templateUrl: './movimientos.component.html',
    styleUrls: ['./movimientos.component.css']
})
export class MovimientosComponent implements OnInit {

    public title: string;
    public movimiento: Movimiento;
    public empleado: Empleado;
    public roles: Rol[];
    public tipos: Tipo[];
    public idMovimiento: number;
    public idEmpleado: number;
    public sueldo: number;
    public tieneEntregas: boolean;
    public cubrioRol: boolean;
    public generado: boolean;

    private fechaActual: any;
    private toasterService: ToasterService;
    private pagoEntrega: number;
    private bono: number[];
    private factorPorcentajeISR: number[];
    private factorPorcentajeVales: number;
    private diasLaborales: number;
    private sueldoHora: number;
    private jornada: number;
    private sueldoDia: number;
    private ingresoMensual: number;
    private pagoTotalEntregas: number;
    private bonoDia: number;
    private totalValesDespensa: number;
    private montoISR: number;
    private montoISRAdicional: number;
    private ingresosDespuesISR: number;

    constructor(
        private empleadosServicio: EmpleadosService,
        private route: ActivatedRoute,
        toasterService: ToasterService,
        private router: Router
    ) {
        this.title = 'Captura de movimientos';
        this.fechaActual = this.obtenerFechaActual();
        this.empleado = { nombre: '', apellido: '', rol: null, tipo: null };
        this.movimiento = { empleado: 0, fecha: this.fechaActual, entregas: 0, rolCubierto: 0 };
        this.toasterService = toasterService;
        this.pagoEntrega = 5;
        this.bono = [10, 5];
        this.factorPorcentajeISR = [0.09, 0.12];
        this.factorPorcentajeVales = 0.04;
        this.diasLaborales = 30;
        this.jornada = 8;
        this.sueldoHora = 30;
        this.tieneEntregas = false;
        this.cubrioRol = false;
        this.generado = false;
        this.sueldoDia = 0;
        this.ingresoMensual = 0;
        this.pagoTotalEntregas = 0;
        this.bonoDia = 0;
        this.totalValesDespensa = 0;
        this.montoISR = 0;
        this.montoISRAdicional = 0;
        this.ingresosDespuesISR = 0;
    }

    ngOnInit() {

        this.obtenerRoles();

        this.obtenerTipos();

        this.route.params.subscribe((params) => {
            this.idEmpleado = +params['id'];
            if (this.idEmpleado) {
                this.empleadosServicio.obtenerEmpleadoPorId(this.idEmpleado).subscribe((resultado) => {
                    this.empleado = resultado.data.empleado[0];
                    this.movimiento.empleado = this.idEmpleado;
                });

                var mes = (new Date().getMonth() + 1);
                var anio = new Date().getFullYear();

                this.empleadosServicio.obtenerMovimiento(this.idEmpleado, mes, anio).subscribe((resultado) => {
                    if (resultado.data.movimiento) {
                        this.movimiento = resultado.data.movimiento[0];
                        if (this.movimiento.entregas > 0) {
                            this.tieneEntregas = true;
                        }
                        if (this.movimiento.rolCubierto != 0) {
                            this.cubrioRol = true;
                        }
                        this.obtenerIngresosDespuesISR();
                        this.generado = true;
                        var toast: Toast = {
                            type: 'warning',
                            title: 'Atención',
                            body: 'Ya fué generdo los movimientos de este mes',
                            showCloseButton: true
                        };
                        this.toasterService.pop(toast);
                    }
                });

            }
        });
    }

    obtenerFechaActual(): string {
        var dia = new Date().getDate().toString();
        var mes = (new Date().getMonth() + 1).toString();
        var anio = new Date().getFullYear().toString();

        if (mes.length < 2) {
            mes = '0' + mes;
        }

        if (dia.length < 2) {
            dia = '0' + dia;
        }

        return [anio, mes, dia].join('-');
    }

    obtenerRoles() {
        this.empleadosServicio.obtenerRoles().subscribe((resultado) => {
            this.roles = resultado.data.roles;
        });
    }

    obtenerTipos() {
        this.empleadosServicio.obtenerTiposEmpleado().subscribe((resultado) => {
            this.tipos = resultado.data.tipos;
        });
    }

    obtenerSueldoDia() {
        this.sueldoDia = this.sueldoHora * this.jornada;
    }

    obtenerPagoEntregas() {
        if (this.tieneEntregas) {
            this.pagoTotalEntregas = this.pagoEntrega * this.movimiento.entregas;
        }
        else {
            this.pagoTotalEntregas = 0;
        }
    }

    obtenerMontoBonoDia() {
        var rol = 0;

        if (this.cubrioRol) {
            rol = this.movimiento.rolCubierto;
        }
        else {
            rol = this.empleado.rol;
        }

        if (rol == 1) {
            this.bonoDia = this.bono[0] * this.jornada;
        }
        else if (rol == 2) {
            this.bonoDia = this.bono[1] * this.jornada;
        }
        else {
            this.bonoDia = 0;
        }
    }

    obtenerIngresoDiarioAntesISR(): number {
        return this.sueldoDia + this.pagoTotalEntregas + this.bonoDia;
    }

    obtenersueldoMensual() {
        this.ingresoMensual = this.obtenerIngresoDiarioAntesISR() * this.diasLaborales;
    }

    obtenerMontoValesDespensa() {
        this.totalValesDespensa = this.ingresoMensual * this.factorPorcentajeVales;
    }

    obtenerMontoISR(): number {
        this.montoISR = (this.ingresoMensual + this.totalValesDespensa) * this.factorPorcentajeISR[0];

        if (this.ingresoMensual > 16000) {
            this.montoISRAdicional = (this.ingresoMensual + this.totalValesDespensa) * this.factorPorcentajeISR[1];
        }

        return this.montoISR + this.montoISRAdicional;
    }

    obtenerIngresosDespuesISR() {
        this.obtenerSueldoDia();
        this.obtenerPagoEntregas();
        this.obtenerMontoBonoDia();
        this.obtenersueldoMensual();
        this.obtenerMontoValesDespensa();

        this.ingresosDespuesISR = this.ingresoMensual + this.totalValesDespensa - this.obtenerMontoISR();
    }

    onSubmit() {
        swal({
            title: 'Esta apunto de generar los movimientos',
            text: "¿Desea continuar?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, generalo!'
        }).then((respuesta) => {
            if (respuesta.value) {
                if (!this.tieneEntregas || (this.tieneEntregas && this.movimiento.entregas > 0)) {
                    this.obtenerIngresosDespuesISR();
                    this.generado = true;
                    this.title = 'Detalle de movimientos';
                    this.empleadosServicio.guardarMovimiento(this.movimiento).subscribe((resultado) => {
                        if (resultado.data.guardo) {
                            var toast: Toast = {
                                type: 'success',
                                title: '¡Excelente!',
                                body: 'Movimiento generados con éxito',
                                showCloseButton: true
                            };
                            this.toasterService.pop(toast);
                        }
                    }, (error) => {
                        var toast: Toast = {
                            type: 'error',
                            title: 'Ups',
                            body: 'Algo salió mal',
                            showCloseButton: true
                        };
                        this.toasterService.pop(toast);
                    });
                }
                else {
                    var toast: Toast = {
                        type: 'information',
                        title: 'Atención',
                        body: 'Debe capturas la cantidad de entregas',
                        showCloseButton: true
                    };
                    this.toasterService.pop(toast);
                }
            }
        })
    }

    goBack() {
        this.router.navigate(['/empleados']);
    }

    cerrarDetalle() {
        this.generado = false;
        this.title = 'Captura de movimientos';
    }

}
