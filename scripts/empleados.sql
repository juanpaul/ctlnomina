-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-07-2018 a las 16:42:49
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `empleados`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `rol` int(11) NOT NULL,
  `tipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id`, `nombre`, `apellido`, `rol`, `tipo`) VALUES
(3, 'Javier', 'Zazueta', 1, 2),
(6, 'Cesar', 'Aramburo', 1, 2),
(7, 'Julio', 'Acosta', 2, 1),
(8, 'Juan Paul', 'Chavez Sierra', 2, 1),
(10, 'Karo', 'Meza', 3, 1),
(11, 'Fernando', 'Uribe', 1, 1),
(12, 'Marco', 'Hauchbaum', 2, 2),
(14, 'Ana', 'Acosta', 3, 1),
(15, 'Lucia', 'Martinez', 3, 2),
(21, 'Fernando', 'Uribe', 1, 2),
(22, 'Endy', 'Campos', 2, 1),
(23, 'Grace', 'Felix', 3, 2),
(25, 'Paul', 'Angulo', 3, 2),
(31, 'Juan', 'Chavez', 1, 1),
(32, 'Pacheco', 'Aldaco', 2, 2),
(40, 'Paul', 'Martinez', 1, 1),
(42, 'Ivan', 'Aramburo', 1, 1),
(43, 'Javier', 'Uribe', 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientos`
--

CREATE TABLE `movimientos` (
  `id` int(11) NOT NULL,
  `empleado` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `entregas` int(11) NOT NULL,
  `rol_cubierto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `movimientos`
--

INSERT INTO `movimientos` (`id`, `empleado`, `fecha`, `entregas`, `rol_cubierto`) VALUES
(1, 2, '2018-07-23', 10, 0),
(2, 3, '2018-07-23', 0, 0),
(3, 0, '2018-07-23', 5, 0),
(4, 4, '2018-07-23', 5, 0),
(5, 4, '2018-07-23', 5, 0),
(6, 4, '2018-07-23', 5, 0),
(7, 6, '2018-07-23', 0, 0),
(8, 11, '2018-07-23', 0, 0),
(9, 16, '2018-07-24', 10, 0),
(10, 22, '2018-07-24', 20, 0),
(11, 21, '2018-07-24', 9, 0),
(12, 7, '2018-07-24', 20, 0),
(13, 15, '2018-07-24', 0, 0),
(14, 14, '2018-07-24', 0, 0),
(15, 10, '2018-07-24', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`) VALUES
(1, 'Chofer'),
(2, 'Cargador'),
(3, 'Auxiliar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposempleado`
--

CREATE TABLE `tiposempleado` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tiposempleado`
--

INSERT INTO `tiposempleado` (`id`, `nombre`) VALUES
(1, 'Interno'),
(2, 'Externo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rol` (`rol`),
  ADD KEY `tipo` (`tipo`);

--
-- Indices de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tiposempleado`
--
ALTER TABLE `tiposempleado`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tiposempleado`
--
ALTER TABLE `tiposempleado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD CONSTRAINT `empleados_ibfk_1` FOREIGN KEY (`rol`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `empleados_ibfk_2` FOREIGN KEY (`tipo`) REFERENCES `tiposempleado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
